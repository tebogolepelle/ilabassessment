package PageObjects;

public class PageObjects {

    private static final String careers ="/html/body/header/div/div/div[3]/nav/ul/li[4]/a";
    private static final String country ="div.vc_btn3-container:nth-child(9)";
    private static final String availableJob ="div.wpjb-grid-row:nth-child(1) > div:nth-child(2) > span:nth-child(1) > a:nth-child(1)";
    private static final String applyOnline = ".wpjb-form-toggle";
    private String applicantName  = "applicant_name";
    private String applicantEmail = "email";
    private static final String applicantPhone = "phone";
    private static final String sendApplication = "#wpjb_submit";

    public String getCareerButton(){
        return careers;
    }
    public String getCountryButton(){
        return country;
    }
    public String getAvailableJobButton(){
        return availableJob;
    }
    public String getApplyOnlineButton(){
        return applyOnline;
    }
    public String getApplicantName(){
        return applicantName;
    }
    public String getApplicantEmail(){
        return applicantEmail;
    }
    public String getApplicantPhone(){
        return applicantPhone;
    }
    public String getSendApplicationButton(){
        return sendApplication;
    }
}

