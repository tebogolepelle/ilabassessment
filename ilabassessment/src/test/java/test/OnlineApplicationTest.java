package test;

import PageObjects.PageObjects;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Test;
import org.openqa.selenium.*;
import Utilities.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

public class OnlineApplicationTest extends DriverSetup {

    public ExcelUtils excelUtils;

    private String[][] data = null;
    private String name;
    private String email;
    public String fileName;

    PageObjects pObjects = new PageObjects();
    TestLogger tLog = new TestLogger();
    String phone = "0";

    @Test
    public void OnlineApplicationTest() throws Exception {

        this.loadExcellFile();

      // Online job application test
        if(wdriver.getTitle().equalsIgnoreCase("Home Page - iLAB"))
        {
            try{
                    wdriver.findElement(By.xpath(pObjects.getCareerButton())).click();
                    ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Career Button Clicked");
            }
            catch (Exception error){
                //Take screen shots for failed step
                Screenshot.captureScreenshots(wdriver,"careers");
                ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when clicking Button "+ error.getMessage());
                tLog.logError(error.getMessage());
            }
        }else{
            tLog.logError("Home Page not found");
        }

        wdriver.findElement(By.xpath(pObjects.getCareerButton())).sendKeys(Keys.PAGE_DOWN);

        //if(wdriver.getTitle().equalsIgnoreCase("CAREERS - iLAB")) {
            try {
               // wdriver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);

                wdriver.findElement(By.cssSelector(pObjects.getCountryButton())).click();
                ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Country Selected");
            } catch (Exception error) {
                Screenshot.captureScreenshots(wdriver, "country selected");
                ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when selecting Country " + error.getMessage());
                tLog.logError(error.getMessage());
            }
//        }else{
//            tLog.logError("CAREERS Page not found");
//        }
        if(wdriver.getTitle().equalsIgnoreCase("SOUTH AFRICA - iLAB")) {
            try {
                wdriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

                wdriver.findElement(By.cssSelector(pObjects.getAvailableJobButton())).click();
                ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Open positions clicked");
            } catch (Exception error) {
                Screenshot.captureScreenshots(wdriver, "available job selected");
                ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when selecting job " + error.getMessage());
                tLog.logError(error.getMessage());
            }
        }else {
            tLog.logError("SOUTH AFRICA Page not found");
        }

        try{
            wdriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            wdriver.findElement(By.cssSelector(pObjects.getApplyOnlineButton())).click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Apply Online button clicked");
        }
        catch(Exception error){
            Screenshot.captureScreenshots(wdriver,"apply online button");
            tLog.logError(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error clicking apply online button "+error.getMessage());
        }
        //Entering of applicant details
        if(wdriver.findElement(By.id(pObjects.getApplicantName())).isDisplayed()) {
            try {
                wdriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

                wdriver.findElement(By.id(pObjects.getApplicantName())).sendKeys(name);
                wdriver.findElement(By.id(pObjects.getApplicantEmail())).sendKeys(email);
                wdriver.findElement(By.id(pObjects.getApplicantPhone())).sendKeys(generateCellNumber());

                ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Applicant details entered successfully");
            } catch (Exception error) {
                Screenshot.captureScreenshots(wdriver, "applicant details");
                tLog.logError(error.getMessage());
                ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Applicant details not entered successfully " + error.getMessage());
            }
        }else{
            tLog.logError("Application details form not successfully displayed");
        }

        //Send application
        try {
            wdriver.findElement(By.cssSelector(pObjects.getSendApplicationButton())).click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Send application button clicked");
        }
        catch(Exception error){
            Screenshot.captureScreenshots(wdriver,"send application");
            tLog.logError(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when entering applicant phone  "+error.getMessage());
        }

        //Validation of message displayed
        try {
            boolean isTheTextDisplayed = wdriver.getPageSource().contains("You need to upload at least one file.");
               assertTrue(isTheTextDisplayed);

            	if(isTheTextDisplayed == true) {
                ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Validation Passed on upload file");
            	}else if(isTheTextDisplayed == false) {

            		ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Validation Failed on upload file");
            	}

                ExtentReport.getInstanceExport().flush();
            	tLog.logInfo("Report written");
        }
        catch(Exception error){
            Screenshot.captureScreenshots(wdriver,"error message");
            tLog.logError(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Validating file upload  "+error.getMessage());
        }
    }

    /**
     * Description: Function to generate a 10 digit phone number
     * @author Tebogo Lepelle
     */
    private String generateCellNumber()
    {
        for(int i=0; i < 9; i++){
            phone = phone + (int)(Math.random()*9+1);
        }
        return phone;
    }

    /**
     * Description: Loads test data from an Excel file
     * @author Tebogo Lepelle
     */
    private void loadExcellFile() throws Exception
    {
        excelUtils = new ExcelUtils();
        fileName = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources"+ File.separator + "TestData.xlsx";

        data = excelUtils.readExcelDataFileToArray(fileName, "PersonalDetails");
        String[] headers = ExcelUtils.getColumHeaders(data);

        ExtentReport.getInstanceExport().startTest("Assessment Test");

        for(int i = 1; i < data.length; i++){

            name = ExcelUtils.getCellValue(data[i], headers, "Name");
            email = ExcelUtils.getCellValue(data[i], headers, "Email");
        }
    }

}
