package Utilities;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ExcelUtils {

    static Logger LOG = Logger.getLogger(ExcelUtils.class);
    private List<String> sheetNamesList = new ArrayList<>();
    private FileInputStream excelFileInputStream;
    private FileOutputStream excelFileOutputStream;
    Workbook workbook;
    private Sheet writeSheet;
    private Row row;
    private int rowNumber;

    public List<String> getSheetNamesList() { return sheetNamesList;
    }

    private void getWorkbook(FileInputStream inputStream, String excelFilePath)throws IOException {

        LOG.info("Reading workbook from file:"+excelFilePath);
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            LOG.error("File extension not provided");
            throw new IllegalArgumentException("Please include the excel file format .e.g xls,xlsx");
        }
    }

    public List readExcelDataFileToList(String dataSheetFileName, String sheetName)throws IOException {

        List<List<String>> twoDdataList = new ArrayList<>();
        LOG.info("Processing datasheet: "+dataSheetFileName);
        LOG.info("Sheet Name: "+sheetName);
        excelFileInputStream = new FileInputStream(new File(dataSheetFileName));
        getWorkbook(excelFileInputStream,dataSheetFileName);
        Sheet dataSheet = workbook.getSheet(sheetName);
        sheetNamesList.add(sheetName);
        Iterator<Row> iterator = dataSheet.iterator();

        List<String> tempList;
        int numOfColumns=0;
        LOG.debug("Reading sheet rows");
        while (iterator.hasNext()){
            Row currentRow = iterator.next();
            //Iterator<Cell> cellIterator = currentRow.iterator();
            if(currentRow.getRowNum()==0){
                LOG.debug("Getting column headers");
                if(currentRow.getLastCellNum()==0){
                    throw new IOException("Expected column headers on the first column. Column headers empty");
                }
                numOfColumns=currentRow.getLastCellNum();
            }
            tempList = getRowDataList(currentRow,numOfColumns);
            //tempList.removeIf(String::isEmpty);//If there is no content on the list. break
            if(tempList.isEmpty() || tempList.size()==0) {
                break;
            }else {
                twoDdataList.add(tempList);
            }
        }
        LOG.debug("Closing file");
        excelFileInputStream.close();
        if(twoDdataList.size()==0)
            throw new IOException("Data sheet is empty. sheet name:"+sheetName);
        return twoDdataList;
    }

    public List<List<List<String>>> readExcelDataFileToList(String dataSheetFileName)throws IOException {

        List<List<List<String>>> dataList = new ArrayList<>();
        //List<List<String>> sheetDataList = new ArrayList<>();
        LOG.info("Processing data sheet: "+dataSheetFileName);
        FileInputStream excelFileStream =null;
        if(this.getClass().getClassLoader().getResource(dataSheetFileName)!=null){
            excelFileStream = new FileInputStream(this.getClass().getClassLoader().getResource(dataSheetFileName).getFile());
        }else if(new File(dataSheetFileName).exists()){
            excelFileStream = new FileInputStream(new File(dataSheetFileName));
        }else{
            throw new IOException("Excel data sheet file not found:"+dataSheetFileName);
        }
        getWorkbook(excelFileStream,dataSheetFileName);
        LOG.info("Getting sheet names in the workbook");
        for(int i =0; i < workbook.getNumberOfSheets();i++){
            LOG.info("Sheet Name: "+workbook.getSheetName(i));
            sheetNamesList.add(workbook.getSheetName(i));
        }

        sheetNamesList.forEach((sheetName)->{
            LOG.info("Processing sheet: "+sheetName);
            Iterator<Row> iterator = workbook.getSheet(sheetName).iterator();
            List<String> rowList;
            int numOfColumns = 0;
            LOG.debug("Reading sheet rows");
            List<List<String>> sheetDataList = new ArrayList<>();
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                //Iterator<Cell> cellIterator = currentRow.iterator();
                if (currentRow.getRowNum() == 0) {
                    LOG.debug("Getting column headers");
                    if (currentRow.getLastCellNum() == 0) {
                        // throw Throwables.propagate("Expected column headers on the first column. Column headers empty");
                    }
                    numOfColumns = currentRow.getLastCellNum();
                }
                rowList = getRowDataList(currentRow, numOfColumns);
                //tempList.removeIf(String::isEmpty);//If there is no content on the list. break
                if (!rowList.isEmpty()) {
                    sheetDataList.add(rowList);
                }
            }
            dataList.add(sheetDataList);
        });
        LOG.debug("Closing file stream");
        excelFileStream.close();
        return dataList;
    }

    private List<String> getRowDataList(Row currentRow, int numOfRowColumns){

        List<String> tempList = new ArrayList<>();
        for(int i = 0;i < numOfRowColumns;i++){
            Cell currentCell = currentRow.getCell(i);
            String cellValue;

            if(currentCell==null){
                cellValue="";
            }
            else {
                //Get the input type
                switch (currentCell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        BigDecimal bigDecimal = new BigDecimal(currentCell.getNumericCellValue());
                        cellValue = bigDecimal.toString().trim();
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        cellValue = Boolean.toString(currentCell.getBooleanCellValue()).trim();
                        break;
                    case Cell.CELL_TYPE_ERROR:
                        cellValue = "";
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        cellValue = currentCell.getCellFormula();
                        break;
                    default:
                        cellValue = currentCell.getStringCellValue().trim();
                }
            }
            tempList.add(cellValue);
        }
        return tempList;
    }

    public String [][] readExcelDataFileToArray(String dataSheetFileName, String sheetName)throws IOException {

        List<List<String>> twoDdataList = readExcelDataFileToList(dataSheetFileName,sheetName);
        LOG.debug("Converting data list to two-d array");
        List<String> innerList=new ArrayList<>();

        //Remove empty rows
        int countValidRows=0;
        for (int i = 0; i < twoDdataList.size(); i++) {
            countValidRows++;
            innerList.addAll(twoDdataList.get(i));
            innerList.removeIf(String::isEmpty);
            if(innerList.size()==0){
                break;
            }
            innerList.clear();
        }
        //Copy to array
        String [][] twoDArray = new String[countValidRows][];
        for (int i = 0; i < twoDArray.length; i++) {
            innerList = twoDdataList.get(i);
            String[] innerAsArray = innerList.toArray(new String[innerList.size()]);
            twoDArray[i] = innerAsArray;
        }

        return  twoDArray;
    }

    public String [][][] readExcelDataFileToArray(String dataSheetFileName)throws IOException {

        List<List<List<String>>> dataList = readExcelDataFileToList(dataSheetFileName);
        LOG.debug("Converting data list to array");
        List<String> innerList;
        List<Integer> validRows= new ArrayList<>();
        //Copy to array
        String [][][] dataArray = new String[dataList.size()][][];
        for (int k =0; k< dataArray.length;k++) {
            dataArray[k]=new String[dataList.get(k).size()][];
            for (int i = 0; i < dataArray[k].length; i++) {
                innerList = dataList.get(k).get(i);
                String [] innerAsArray = innerList.toArray(new String[innerList.size()]);
                //  dataArray[k][i] = innerAsArray;
                dataArray[k][i] = innerAsArray;
            }
        }
        return  dataArray;
    }

    public static String getCellValue(String [] dataRowArray,String [] headers, String columnName)throws Exception {

        LOG.debug(String.format("Getting cell value for column %s",columnName));
        if(dataRowArray.length==0){
            throw new Exception("Data provided is empty");
        }
        if(headers.length==0){
            throw new Exception("Headers are empty");
        }
        if(dataRowArray.length!=headers.length){
            throw new Exception("The number of column headers should match the data entries count provided");
        }
        int columnIndex = Arrays.asList(headers).indexOf(columnName.trim());
        LOG.debug("Column index is:"+columnIndex);
        if(columnIndex<0){
            LOG.error(String.format("Column index %s does not exist in the excel sheet",columnIndex));
            throw new Exception("Column name not found");
        }
        return dataRowArray[columnIndex].toString();
    }

    public static int getColumnIndex(String [] headers, String columnName)throws Exception {

        LOG.debug(String.format("Getting cell index for header %s",columnName));
        if(headers.length==0){
            throw new Exception("Headers are empty");
        }
        int columnIndex = Arrays.asList(headers).indexOf(columnName.trim());
        LOG.debug("Column index is:"+columnIndex);
        if(columnIndex<0){
            LOG.error(String.format("Column index %s does not exist in the headers provided sheet",columnIndex));
            throw new Exception(String.format("Column index %s does not exist in the headers provided sheet",columnIndex));
        }
        return columnIndex;
    }

    public static String [] getColumHeaders(String [][] dataArray)throws Exception {

        LOG.debug("Getting column headers");
        if(dataArray==null || dataArray[0]==null || dataArray[0].length==0){
            LOG.error("No column headers found, data is empty");
            throw new Exception("No column headers found");
        }
        return dataArray[0];
    }

    public void writeHeaders(String [] headers){

        if(headers==null){
            throw new IllegalArgumentException("Please provide report headers");
        }
        row = writeSheet.createRow(rowNumber++);
        for (int i=0;i<headers.length;i++) {
            writeToCell(headers[i],i);
        }
    }

    public void flush(){
        try {
            workbook.write(excelFileOutputStream);
            excelFileOutputStream.flush();
            excelFileOutputStream.close();
        }catch (IOException ioE){
            LOG.error(ioE.getMessage());
        }
    }

    public void writeToCell(String value, int cellNumber) {
        Cell cell = row.createCell(cellNumber);
        cell.setCellValue(value);
    }

    public void writeToReport(String [] data) {
        for (int i=0;i<data.length;i++) {
            writeToCell(data[i], i);
        }
    }

    public void createRow() {
        row = writeSheet.createRow(rowNumber++);
    }

    public void writeToReport(Object [][] data) {
        for (Object[] datatype : data) {
            createRow();
            int colNum = 0;
            for (Object field : datatype) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
        }
        flush();
    }

    public void createExcelWorkbook(String fileName){
        createExcelWorkbook(fileName,"Sheet1");

    }

    public void createExcelWorkbook(String fileName, String sheetName){
        try {
            excelFileOutputStream = new FileOutputStream(new File(fileName));
            workbook = new XSSFWorkbook();
            writeSheet = workbook.createSheet(sheetName);
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage());
        }
    }
}

