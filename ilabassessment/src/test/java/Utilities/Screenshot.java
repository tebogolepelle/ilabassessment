package Utilities;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class Screenshot {

    /**
     * Description: Captures the screenshot
     * @author Tebogo Lepelle
     */
    public static void captureScreenshots(WebDriver driver,String screenShotName)  {
        TestLogger tLog = new TestLogger();
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(source,new File("./Screenshots/"+screenShotName+".png"));
        } catch (Exception e) {
            tLog.logError("Exception while taking screenshot" + e.getMessage());
        }
    }
}
