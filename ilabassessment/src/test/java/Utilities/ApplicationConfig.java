//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package Utilities;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

public class ApplicationConfig {
    private String configFile = "config.properties";
    private String reportingDirectory;
    private String reportName;
    private String browser;
    private int timeout;
    private String browserBinaryPath;
    private String environment;
    private String inputType;
    private String inputFile;
    private Properties configProps;
    private String loggerName;
    private String log4JFile;
    private int waitTimeout;

    public ApplicationConfig(String configFile) {
        this.configFile = configFile;

        try {
            this.loadProperties();
        } catch (FileNotFoundException var3) {
            this.generateDefaultProperties();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    public ApplicationConfig() {
        this.generateDefaultProperties();
    }

    private void generateDefaultProperties() {
        try {
            this.writeProperties();
            this.loadPropertiesIntoClass();
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    private void loadProperties() throws IOException {
        InputStream inputStream = new FileInputStream(this.configFile);
        if (this.configProps == null) {
            this.configProps = new Properties();
            this.loadPropertiesIntoClass();
        }

        this.configProps.load(inputStream);
    }

    private void writeProperties() throws IOException {
        this.configProps = new Properties();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh_m_mss");
        Calendar calendar = GregorianCalendar.getInstance();
        this.configProps.setProperty(ApplicationConfigEnum.ReportDirectory.toString(), System.getProperty("user.dir") + File.separator + "Output" + File.separator + "Test_Run_Output_" + simpleDateFormat.format(calendar.getTime()) + File.separator);
        this.configProps.setProperty("log4jFile", System.getProperty("user.dir") + File.separator + "Output" + File.separator + "Test_Run_Output_" + simpleDateFormat.format(calendar.getTime()) + File.separator + "Test_Run_Logger.log");
        this.configProps.setProperty(ApplicationConfigEnum.Browser.toString(), "chrome");
        this.configProps.setProperty(ApplicationConfigEnum.BrowserBinaryPath.toString(), System.getProperty("user.dir") + "src" + File.separator + "test" + File.separator + "resources" + File.pathSeparator + "drivers" + File.separator + "chromedriver.exe");
        this.configProps.setProperty(ApplicationConfigEnum.InputType.toString(), "");
        this.configProps.setProperty(ApplicationConfigEnum.Timeout.toString(), "20");
        this.configProps.setProperty(ApplicationConfigEnum.WaitTimeout.toString(), "60");
        this.configProps.setProperty("LoggerName", "DefaultLogger");
        this.configProps.store(new FileOutputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config" + File.separator + "defaultconfig.properties"), "default properties generated");
    }

    private void loadPropertiesIntoClass() {
        this.reportingDirectory = this.configProps.getProperty(ApplicationConfigEnum.ReportDirectory.toString());
        this.loggerName = this.configProps.getProperty("LoggerName");
        this.log4JFile = this.configProps.getProperty("log4jFile");
        this.browser = this.configProps.getProperty(ApplicationConfigEnum.Browser.toString());
        this.browserBinaryPath = this.configProps.getProperty(ApplicationConfigEnum.BrowserBinaryPath.toString());
        this.inputType = this.configProps.getProperty(ApplicationConfigEnum.InputType.toString());
        this.inputFile = this.configProps.getProperty(ApplicationConfigEnum.InputFile.toString());
        this.timeout = Integer.parseInt(this.configProps.getProperty(ApplicationConfigEnum.Timeout.toString()));
        this.waitTimeout = Integer.parseInt(this.configProps.getProperty(ApplicationConfigEnum.WaitTimeout.toString()));
    }

    public String getReportingDirectory() {
        return this.reportingDirectory;
    }

    public String getLoggerName() {
        return this.loggerName;
    }

    public String getLog4JFile() {
        return this.log4JFile;
    }
}
