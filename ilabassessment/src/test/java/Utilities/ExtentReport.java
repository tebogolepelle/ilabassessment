package Utilities;

import java.io.File;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentReport {

    public static ExtentReports extent;
    public static ExtentTest logger;

    public static ExtentReports getInstanceExport() {
        if (extent == null) {
            extent = new ExtentReports(System.getProperty("user.dir")+ File.separator + "report"+ File.separator +"IlabAssessment.html");
            extent.loadConfig(new File(System.getProperty("user.dir")+ File.separator +"extent-config.xml"));
        }
        return extent;
    }

    public static ExtentTest getInstanceExtentTest() {
        if (logger == null) {
            logger = extent.startTest("Pass-IlabAssessment");
        }
        return logger;
    }

}