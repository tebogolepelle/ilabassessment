
package Utilities;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;

public class TestLogger {
    private Logger logger;

    public TestLogger(ApplicationConfig applicationConfig) {
        new SimpleDateFormat("yyyy-MM-dd hhmmss");
        System.setProperty("logFile", applicationConfig.getReportingDirectory() + "Test_Run_Logger.log");
        this.logger = Logger.getLogger(TestLogger.class);
    }

    public TestLogger() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        this.logger = Logger.getLogger(applicationConfig.getLoggerName());
    }

    public void logInfo(String info) {
        this.logger.info(info);
    }

    public void logError(String error) {
        this.logger.error(error);
    }


    public void logFatal(String fatal) {
        this.logger.fatal(fatal);
    }
}
