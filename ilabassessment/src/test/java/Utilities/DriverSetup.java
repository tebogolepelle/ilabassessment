package Utilities;

import org.junit.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.*;
import java.util.Properties;

public class DriverSetup {
    public static WebDriver wdriver;
    public String strOSName = "";
    public String strProjectLoc;

    private static final String WEBDRIVERCHROME = "webdriver.chrome.driver";
    private static final String BROWSERDRIVERS = "BrowserDrivers";
    private String driverBase = "." + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "drivers" + File.separator;
    Properties prop = new Properties();
    TestLogger tLog = new TestLogger();

    /**
     * Description: Launches the browser and navigates to the iLab website
     * @author Tebogo Lepelle
     */
    @Before
    public void setup(){

        String strCapAppPathName="";
        String strBrowser = "";
        String strHeadless;

        this.loadPropertyFile();

        ApplicationConfig applicationConfig = new ApplicationConfig();
        System.setProperty("logFile",applicationConfig.getLog4JFile());

        strBrowser = prop.getProperty("browser.name");
        strCapAppPathName = prop.getProperty("cap.App");
        strHeadless = prop.getProperty("browser.headless");

        // ********************************************this code is for Google chrome*******************************************
        if (strBrowser.equals("GC"))
        {
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("useAutomationExtension", false);
            if (strHeadless.equals("true")) {
                options.addArguments("headless");
            }

            if (strOSName.equalsIgnoreCase("Mac OS X"))
            {
                System.setProperty(WEBDRIVERCHROME, driverBase + "chromedriver");

                wdriver = new ChromeDriver(options);
                wdriver.get(strCapAppPathName);
                Dimension d = new Dimension(1400,2000);
                wdriver.manage().window().setSize(d);
            }
            else
            {
                System.setProperty(WEBDRIVERCHROME, driverBase + "chromedriver.exe");

                wdriver = new ChromeDriver(options);
                wdriver.get(strCapAppPathName);
                wdriver.manage().window().maximize();
            }
        } else if (strBrowser.equals("EDGE"))
        {
        // ********************************************this code is for EDGE*******************************************
            if (strOSName.equalsIgnoreCase("Mac OS X"))
            {
                System.setProperty("webdriver.edge.driver", driverBase + "MicrosoftWebDriver");

                wdriver = new EdgeDriver();
                wdriver.get(strCapAppPathName);
                Dimension d = new Dimension(1400,2000);
                wdriver.manage().window().setSize(d);
            }
            else
            {
                System.setProperty("webdriver.edge.driver", driverBase + "MicrosoftWebDriver.exe");

                wdriver = new EdgeDriver();
                wdriver.get(strCapAppPathName);
                wdriver.manage().window().maximize();
            }

        }
    }

    //Closing the browser after the test
    @After
    public void endTest(){
        wdriver.close();
    }

    /**
     * Description: Function to load the properties file
     * @author Tebogo Lepelle
     */
    private void loadPropertyFile(){
        InputStream input;
        try {
            strProjectLoc = System.getProperty("user.dir");
            strOSName = System.getProperty("os.name");

            System.out.println(strProjectLoc + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config" + File.separator + "config.properties");
            input = new FileInputStream(strProjectLoc + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config" + File.separator + "config.properties");
            prop.load(input);
        } catch (FileNotFoundException e) {
            tLog.logError("File not found");
        } catch (IOException e) {
            tLog.logError("IO Exception");
        }
    }
}
