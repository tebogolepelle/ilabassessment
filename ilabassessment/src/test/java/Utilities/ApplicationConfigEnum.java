package Utilities;

public enum ApplicationConfigEnum {
    ReportDirectory,
    ReportName,
    Browser,
    Timeout,
    WaitTimeout,
    BrowserBinaryPath,
    Environment,
    InputType,
    InputFile;

    private ApplicationConfigEnum() {
    }
}
